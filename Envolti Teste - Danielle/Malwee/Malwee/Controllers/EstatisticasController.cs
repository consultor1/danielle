﻿using Malwee.Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Malwee.Controllers
{
    public class EstatisticasController : Controller
    {
        // GET: Estatisticas
        // NÃO É NECESSARIO ESTÁ LOGADO, PORTANTO NÃO HERDA O BASECONTROLLER

            //INSTANCIA O DAO, ONDE É COLOCADO AS QUERYS PARA OS RELATORIOSS
        EstatisticaDAO Dao = new EstatisticaDAO();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClienteMaisGastaramMes()
        {
            //BUSCA TODOS OS CLIENTES ORDENANDO POR MÊS
            var SelecionarClientesPorMes = Dao.SelecionarClientesPorMes().ToList();
            //janeiro 
            ViewBag.Janeiro = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 1).Take(3).ToList();
            //fevereiro
            ViewBag.Fevereiro = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 2).Take(3).ToList();
            //marco
            ViewBag.Marco = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 3).Take(3).ToList();
            //Abril
            ViewBag.Abril = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 4).Take(3).ToList();
            //Maio
            ViewBag.Maio = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 5).Take(3).ToList();
            //Junho
            ViewBag.Junho = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 6).Take(3).ToList();
            //Julho
            ViewBag.Julho = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 7).Take(3).ToList();
            //Agosto
            ViewBag.Agosto = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 8).Take(3).ToList();
            //Setembro
            ViewBag.Setembro = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 9).Take(3).ToList();
            //Outubro
            ViewBag.Outubro = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 10).Take(3).ToList();
            //Novembro
            ViewBag.Novembro = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 11).Take(3).ToList();
            //Dezembro
            ViewBag.Dezembro = Dao.SelecionarClientesPorMes().Where(c => c.Mes == 12).Take(3).ToList();


            return View();
        }
       
        public ActionResult FornecedorSemAtendimentoNoMes()
        {

            //janeiro 
            ViewBag.Janeiro = Dao.FornecedoresSemAtendimeto(1).ToList();
            //fevereiro
            ViewBag.Fevereiro = Dao.FornecedoresSemAtendimeto(2).ToList();
            //marco
            ViewBag.Marco = Dao.FornecedoresSemAtendimeto(3).ToList();
            //Abril
            ViewBag.Abril = Dao.FornecedoresSemAtendimeto(4).ToList();
            //Maio
            ViewBag.Maio = Dao.FornecedoresSemAtendimeto(5).ToList();
            //Junho
            ViewBag.Junho = Dao.FornecedoresSemAtendimeto(6).ToList();
            //Julho
            ViewBag.Julho = Dao.FornecedoresSemAtendimeto(7).ToList();
            //Agosto
            ViewBag.Agosto = Dao.FornecedoresSemAtendimeto(8).ToList();
            //Setembro
            ViewBag.Setembro = Dao.FornecedoresSemAtendimeto(9).ToList();
            //Outubro
            ViewBag.Outubro = Dao.FornecedoresSemAtendimeto(10).ToList();
            //Novembro
            ViewBag.Novembro = Dao.FornecedoresSemAtendimeto(11).ToList();
            //Dezembro
            ViewBag.Dezembro = Dao.FornecedoresSemAtendimeto(12).ToList();


            return View();
        }
    }
}