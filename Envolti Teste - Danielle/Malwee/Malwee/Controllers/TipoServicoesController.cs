﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Malwee.Models;

namespace Malwee.Controllers
{
    public class TipoServicoesController : Controller
    {
        private MalweeEntities3 db = new MalweeEntities3();

        // GET: TipoServicoes
        public ActionResult Index()
        {
            var tipoServico = db.TipoServico.Include(t => t.Fornecedor);
            return View(tipoServico.ToList());
        }

        //RESPOSTA VIA AJAX
        public JsonResult SelecionarServico()
        {
            var lista = db.TipoServico.Select(x => new { x.Descricao, x.Id }).ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        // GET: TipoServicoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoServico tipoServico = db.TipoServico.Find(id);
            if (tipoServico == null)
            {
                return HttpNotFound();
            }
            return View(tipoServico);
        }

        // GET: TipoServicoes/Create
        public ActionResult Create()
        {
            ViewBag.IdFornecedor = new SelectList(db.Fornecedor, "Id", "Nome");
            return View();
        }

        // POST: TipoServicoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Descricao,IdFornecedor")] TipoServico tipoServico)
        {
            if (ModelState.IsValid)
            {
                db.TipoServico.Add(tipoServico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdFornecedor = new SelectList(db.Fornecedor, "Id", "Nome", tipoServico.IdFornecedor);
            return View(tipoServico);
        }

        // GET: TipoServicoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoServico tipoServico = db.TipoServico.Find(id);
            if (tipoServico == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFornecedor = new SelectList(db.Fornecedor, "Id", "Nome", tipoServico.IdFornecedor);
            return View(tipoServico);
        }

        // POST: TipoServicoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Descricao,IdFornecedor")] TipoServico tipoServico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoServico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFornecedor = new SelectList(db.Fornecedor, "Id", "Nome", tipoServico.IdFornecedor);
            return View(tipoServico);
        }

        // GET: TipoServicoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoServico tipoServico = db.TipoServico.Find(id);
            if (tipoServico == null)
            {
                return HttpNotFound();
            }
            return View(tipoServico);
        }

        // POST: TipoServicoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoServico tipoServico = db.TipoServico.Find(id);
            db.TipoServico.Remove(tipoServico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
