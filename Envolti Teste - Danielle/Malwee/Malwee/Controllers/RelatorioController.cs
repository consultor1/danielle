﻿using Malwee.Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Malwee.Controllers
{
    public class RelatorioController : BaseController
    {
        // GET: Relatorio
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Listar(int cliente, int tipoServico, string cidade
                                    , string estado, string bairro, string vlmin, string vlmax
                                )
        {
            RelatorioDAO Dao = new RelatorioDAO();
            var listaRecurso = Dao.Listar(cliente, tipoServico, cidade
                                    , estado, bairro, vlmin, vlmax).ToList();
            return Json(new { rows = listaRecurso }, JsonRequestBehavior.AllowGet);
        }
    }
}