﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Malwee.Models;

namespace Malwee.Controllers
{
    //TODO CONTROLADOR PASSARÁ A HERDAR DO CONTROLLER BASECONTROLLER
    //DESSA FORMA TEMOS A CERTEZA QUE SÓ USUARIOS LOGADOS ENTRÃO NOS MÉTODOS
    public class ServicoPrestadoesController : BaseController
    {
        private MalweeEntities3 db = new MalweeEntities3();

        // GET: ServicoPrestadoes
        public ActionResult Index()
        {
            var servicoPrestado = db.ServicoPrestado.Include(s => s.Cliente).Include(s => s.TipoServico);
            return View(servicoPrestado.ToList());
        }

        // GET: ServicoPrestadoes/Details/5
     

        // GET: ServicoPrestadoes/Create
        public ActionResult Create()
        {
            ViewBag.IdCliente = new SelectList(db.Cliente.OrderBy(s=> s.Nome.ToUpper()), "Id", "Nome");
            ViewBag.IdServico = new SelectList(db.TipoServico.OrderBy(s=>s.Descricao.ToUpper()), "Id", "Descricao");
            return View();
        }

        // POST: ServicoPrestadoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdServico,IdCliente,DtAtendimento,ValorServico,Descricao")] ServicoPrestado servicoPrestado)
        {
            if (ModelState.IsValid)
            {
                db.ServicoPrestado.Add(servicoPrestado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = new SelectList(db.Cliente.OrderBy(s=>s.Nome.ToUpper()), "Id", "Nome", servicoPrestado.IdCliente);
            ViewBag.IdServico = new SelectList(db.TipoServico.OrderBy(s=>s.Descricao.ToUpper()), "Id", "Descricao", servicoPrestado.IdServico);
            return View(servicoPrestado);
        }

        // GET: ServicoPrestadoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicoPrestado servicoPrestado = db.ServicoPrestado.Find(id);
            if (servicoPrestado == null)
            {
                return HttpNotFound();
            }
          
            DateTime Data;
            Data = Convert.ToDateTime(servicoPrestado.DtAtendimento);
            string DtAtendimento = Data.ToString("dd/MM/yyyy");
            ViewBag.DtAtendimento = DtAtendimento;
            
            ViewBag.IdCliente = new SelectList(db.Cliente, "Id", "Nome", servicoPrestado.IdCliente);
            ViewBag.IdServico = new SelectList(db.TipoServico, "Id", "Descricao", servicoPrestado.IdServico);
            return View(servicoPrestado);
        }

        // POST: ServicoPrestadoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdServico,IdCliente,DtAtendimento,ValorServico,Descricao")] ServicoPrestado servicoPrestado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicoPrestado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCliente = new SelectList(db.Cliente, "Id", "Nome", servicoPrestado.IdCliente);
            ViewBag.IdServico = new SelectList(db.TipoServico, "Id", "Descricao", servicoPrestado.IdServico);
            return View(servicoPrestado);
        }

        // GET: ServicoPrestadoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicoPrestado servicoPrestado = db.ServicoPrestado.Find(id);
            if (servicoPrestado == null)
            {
                return HttpNotFound();
            }
            return View(servicoPrestado);
        }

        // POST: ServicoPrestadoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServicoPrestado servicoPrestado = db.ServicoPrestado.Find(id);
            db.ServicoPrestado.Remove(servicoPrestado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
