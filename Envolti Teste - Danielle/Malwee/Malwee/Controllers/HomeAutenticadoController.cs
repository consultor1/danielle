﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Malwee.Controllers
{
    //TODO CONTROLADOR PASSARÁ A HERDAR DO CONTROLLER BASECONTROLLER
    //DESSA FORMA TEMOS A CERTEZA QUE SÓ USUARIOS LOGADOS ENTRÃO NOS MÉTODOS
    public class HomeAutenticadoController : BaseController
    {
        // GET: HomeAutenticado
        public ActionResult Index()
        {
            return View();
        }
    }
}