﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Malwee.Models;

namespace Malwee.Controllers
{
    public class HomeController : Controller
    {
        private MalweeEntities3 db = new MalweeEntities3();
        public ActionResult Index()
        {
            // TELA DE LOGIN, CASO TENHA LOGIN REDIRECIONA PRA PRINCIPAL, SENÃO, CRIE SEU LOGIN
            Session["login"] = "";
            return View();
        }
        public ActionResult Logar(object sender, EventArgs e)
        {
         
            try
            {

                string login = Request.Form["login"];
                string senha = Request.Form["senha"];

                ViewBag.lista  = db.Login.ToList();

                var Usuario = db.Login.FirstOrDefault(x => x.Usuario == login && x.Senha == senha);

                    if (Usuario != null)
                    {
                        //Existe o usuário
                        Session["login"] = login;
                        return RedirectToAction("Index", "HomeAutenticado");
                    }
                    else
                    {
                        //Se voltar nulo, não existe o usuário com esta senha
                        TempData["erro"] ="LOGIN OU SENHA INVÁLIDO";
                        Session["login"] = "";
                        return RedirectToAction("Index", "Home");
                    }
                

               

            }
            catch (Exception ex)
            {
                TempData["erro"] = ex.Message;
                return RedirectToAction("Index", "Home");
            }
        }


    }
}