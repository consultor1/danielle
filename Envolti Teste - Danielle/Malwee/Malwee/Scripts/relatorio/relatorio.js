﻿$(document).ready(function () {
  
    CarregaCliente();
    CarregaTipoServico();

    function CarregaCliente() {
        $.ajax({
            url: "/Clientes/SelecionarCliente",

            success: function (data) {

                $("#cliente").empty();
                $("#cliente").append('<option value=0>Todos</option>');
                $.each(data, function (i, element) {
                    $("#cliente").append('<option value=' + element.Id + '>' + element.Nome + '</option>');
                });
            }
        });
    }

    function CarregaTipoServico() {
        $.ajax({
            url: "/TipoServicoes/SelecionarServico",
            async: false,
            success: function (data) {

                $("#tipoServico").empty();
                $("#tipoServico").append('<option value=0>Todos</option>');
              
                $.each(data, function (i, element) {
                    $("#tipoServico").append('<option value=' + element.Id + '>' + element.Descricao + '</option>');
                });
            }
        });
    }
    $('select#cliente').on("change", function () {
        var id = $("#cliente").val();
   //     $("#nome").val('');
        $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
    });
    $('select#tipoServico').on("change", function () {
        $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
    });

    const inputEleestado = document.getElementById('estado');
    inputEleestado.addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if (key == 13) { // codigo da tecla enter
            nome = this.value;
            $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }

    });
    const inputElecidade = document.getElementById('cidade');
    inputElecidade.addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if (key == 13) { // codigo da tecla enter
            nome = this.value;
            $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }
    });
    const inputElebairro = document.getElementById('bairro');
    inputElebairro.addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if (key == 13) { // codigo da tecla enter
            nome = this.value;
            $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }
    });
    const inputvlmax = document.getElementById('vlmax');
    inputvlmax.addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if (key == 13) { // codigo da tecla enter
            nome = this.value;
            $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }
    });
    const inputvlmin = document.getElementById('vlmin');
    inputvlmin.addEventListener('keyup', function (e) {
        var key = e.which || e.keyCode;
        if (key == 13) { // codigo da tecla enter
            nome = this.value;
            $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }
    });


    //=============================GRID ==================================
    $(function () {

        $grid = $("#jqGrid").jqGrid({
            url: '/Relatorio/Listar',
            mtype: 'GET',
            datatype: 'json',
            postData: {
                cliente: function () { return jQuery("#cliente option:selected").val(); },
                tipoServico: function () { return jQuery("#tipoServico option:selected").val(); },
                cidade: function () { return jQuery("#cidade").val(); },
                estado: function () { return jQuery("#estado").val(); },
                bairro: function () { return jQuery("#bairo").val(); },
                vlmin: function () { return jQuery("#vlmin").val(); },
                vlmax: function () { return jQuery("#vlmax").val(); },

            },
            colModel: [
                { label: 'Cliente', name: 'Cliente', width: 250 },
                { label: 'Tipo Servico', name: 'TipoServico', width: 120 },
                { label: 'Cidade', name: 'Cidade', width: 100 },
                { label: 'Estado', name: 'Estado', width: 60 },
                { label: 'Bairro', name: 'Bairro', width: 80 },
                { label: 'Valor', name: 'Valor', width: 80 },
                { label: 'DtAtendimento', name: 'DtAtendimento', width: 80 },
            ],
            loadonce: true,
            pager: '#jqGridPager',
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            viewrecords: true,
            height: 200
        });
        $("#jqGrid").jqGrid('navGrid', '#jqGridPager', { edit: false, add: false, del: false })


        var $pager = $grid.closest(".ui-jqgrid").find(".ui-pg-table");
        //replace paging icons
        $pager.find(".ui-pg-button>span.ui-icon-seek-first")
            .removeClass("ui-icon ui-icon-seek-first")
            .addClass("fa fa-fast-backward");
        $pager.find(".ui-pg-button>span.ui-icon-seek-prev")
            .removeClass("ui-icon ui-icon-seek-prev")
            .addClass("fa fa-step-backward");
        $pager.find(".ui-pg-button>span.ui-icon-seek-next")
            .removeClass("ui-icon ui-icon-seek-next")
            .addClass("fa fa-step-forward");
        $pager.find(".ui-pg-button>span.ui-icon-seek-end")
            .removeClass("ui-icon ui-icon-seek-end")
            .addClass("fa fa-fast-forward");

        //replace search and refresh icons
        $pager.find(".ui-pg-div>span.ui-icon-search")
            .removeClass("ui-icon ui-icon-search")
            .addClass("fa fa-search");
        $pager.find(".ui-pg-div>span.ui-icon-refresh")
            .removeClass("ui-icon ui-icon-refresh")
            .addClass("fa fa-refresh");
    });
});