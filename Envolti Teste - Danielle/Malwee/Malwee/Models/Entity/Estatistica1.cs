﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Malwee.Models.Entity
{
    public class Estatistica1
    {
        public int Mes { get; set; }
        public string MesDesc { get; set; }
        public string Cliente { get; set; }
        public string Valor { get; set; }
        public string Fornecedor { get; set; }
    }
}