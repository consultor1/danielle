﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Malwee.Models.Entity
{
    public class Relatorio
    {
        public string Cliente { get;set; }
        public string Estado { get;set; }
        public string Cidade { get;set; }
        public string Bairro { get;set; }
        public string TipoServico { get;set; }
        public string Valor { get;set; }
        public string DtAtendimento { get;set; }
    }
}