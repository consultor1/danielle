﻿using Malwee.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Malwee.Models.DAO
{
    public class EstatisticaDAO
    {
        string conexao = WebConfigurationManager.ConnectionStrings["Malwee"].ConnectionString;

        public List<Estatistica1> SelecionarClientesPorMes()
        {
            string consulta = @"  SELECT 
	                                c.Nome,
	                                s.ValorServico,
									s.DtAtendimento, MONTH(s.DtAtendimento) Mes
                                FROM ServicoPrestado s
                                JOIN Cliente c on (s.IdCliente = c.Id)
                                JOIN TipoServico t on (t.Id = s.IdServico)
                            WHERE 1=1
                                AND YEAR(s.DtAtendimento) = 2019  
                                   order by MONTH(s.DtAtendimento), s.ValorServico desc";

            using (var conn = new SqlConnection(conexao))
            {
                var cmd = new SqlCommand(consulta, conn);
                List<Estatistica1> dados = new List<Estatistica1>();
                Estatistica1 p = null;
                try
                {
                    conn.Open();
                    using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            p = new Estatistica1();
                            p.Mes = (int)reader["Mes"];
                            p.Cliente = reader["Nome"].ToString();
                            p.Valor = reader["ValorServico"].ToString();
                            dados.Add(p);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                return dados;
            }
        }
        public List<Estatistica1> FornecedoresSemAtendimeto(int mes)
        {
            string consulta = @" SELECT 
	                                f.Id, f.Nome fornecedor
	                                FROM Fornecedor f
                                WHERE f.id not in 
                                (
	                                SELECT f1.Id 
                                        FROM ServicoPrestado s
		                                JOIN TipoServico t on (t.Id = s.IdServico)
		                                 JOIN Fornecedor f1 on (f1.Id = t.IdFornecedor)
		                            WHERE MONTH(s.DtAtendimento) = "+mes+" ) ";

            using (var conn = new SqlConnection(conexao))
            {
                var cmd = new SqlCommand(consulta, conn);
                List<Estatistica1> dados = new List<Estatistica1>();
                Estatistica1 p = null;
                try
                {
                    conn.Open();
                    using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            p = new Estatistica1();
                            p.Fornecedor = reader["fornecedor"].ToString().ToUpper();                            
                            dados.Add(p);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                return dados;
            }
        }
    }
}