﻿using Malwee.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Malwee.Models.DAO
{
    public class RelatorioDAO
    {
        string conexao = WebConfigurationManager.ConnectionStrings["Malwee"].ConnectionString;

        public List<Cliente> SelecionarCliente()
        {
            string consulta = @" SELECT
                                    Id,
                                    Nome
                                FROM
                                    Cliente
                                                               
                                ORDER BY
                                    Nome";

            using (var conn = new SqlConnection(conexao))
            {
                var cmd = new SqlCommand(consulta, conn);
                List<Cliente> dados = new List<Cliente>();
                Cliente p = null;
                try
                {
                    conn.Open();
                    using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            p = new Cliente();
                            p.Id = (int)reader["Id"];
                            p.Nome = reader["Nome"].ToString();
                            dados.Add(p);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                return dados;
            }
        }

        public List<Relatorio> Listar(int cliente, int tipoServico, string cidade
                                    , string estado, string bairro, string vlmin, string vlmax
                                )
        {
            string consulta = @" SELECT 
	                                c.Nome,
	                                c.Bairro,
	                                c.Cidade,
	                                c.Estado,
	                                t.Descricao,
	                                s.DtAtendimento,
	                                s.Descricao servico,
	                                s.ValorServico

                                FROM ServicoPrestado s
                                JOIN Cliente c on (s.IdCliente = c.Id)
                                JOIN TipoServico t on (t.Id = s.IdServico)
                            WHERE 1=1
                                  ";

            if (cliente > 0)
                consulta = consulta + " AND c.Id = " + cliente;

            if (tipoServico > 0)
                consulta = consulta + " AND s.IdServico = " + tipoServico;

            if (!String.IsNullOrEmpty(cidade))
                consulta = consulta + " AND UPPER(c.Cidade) like '%" + cidade.ToUpper() + "%'  ";

            if (!String.IsNullOrEmpty(bairro))
                consulta = consulta + " AND UPPER(c.Bairro) like '%" + bairro.ToUpper() + "%'  ";

            if (!String.IsNullOrEmpty(estado))
                consulta = consulta + " AND UPPER(c.Estado) like '%" + estado.ToUpper() + "%'  ";


            if (!String.IsNullOrEmpty(vlmin))
                consulta = consulta + " AND s.ValorServico >= '" + vlmin + "'  ";

            if (!String.IsNullOrEmpty(vlmax))
                consulta = consulta + " AND s.ValorServico <= '" + vlmax + "'  ";

            //consulta = consulta + " ORDER BY  D2_PEDIDO desc";

            using (var conn = new SqlConnection(conexao))

            {
                var cmd = new SqlCommand(consulta, conn);
                List<Relatorio> dados = new List<Relatorio>();
                Relatorio p = null;
                try
                {
                    conn.Open();
                    using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            p = new Relatorio();

                            p.Bairro = reader["Bairro"].ToString().ToUpper();
                            p.Cidade = reader["Cidade"].ToString().ToUpper();
                            p.Cliente = reader["Nome"].ToString().ToUpper();
                            p.Estado = reader["Estado"].ToString().ToUpper();
                            p.TipoServico = reader["servico"].ToString().ToUpper();
                            p.Valor = reader["ValorServico"].ToString();
                            p.DtAtendimento = Convert.ToDateTime(reader["DtAtendimento"]).ToString("dd/MM/yyyy");                         
                        
                            dados.Add(p);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                return dados;
            }
        }

    }
}